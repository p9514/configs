CREATE TABLE `Users` (
  `IdUser` BIGINT NOT NULL AUTO_INCREMENT,
  `Email` VARCHAR(255) NOT NULL,
  `FirstName` VARCHAR(255) NOT NULL,
  `LastName` VARCHAR(255) NOT NULL,
  `Phone` VARCHAR(15) NULL,
  `RoleId` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`IdUser`));

CREATE TABLE `UserAddresses` (
  `IdAddress` BIGINT NOT NULL AUTO_INCREMENT,
  `IdUser` BIGINT NULL,
  `Country` VARCHAR(255) NOT NULL,
  `City` VARCHAR(255) NOT NULL,
  `Street` VARCHAR(255) NOT NULL,
  `Number` VARCHAR(45) NULL,
  PRIMARY KEY (`IdAddress`),
  INDEX `From_Address_To_User_FK_idx` (`IdUser` ASC) VISIBLE,
  CONSTRAINT `From_Address_To_User_FK`
    FOREIGN KEY (`IdUser`)
    REFERENCES `Users` (`IdUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `DonationRequests` (
  `IdRequest` BIGINT NOT NULL AUTO_INCREMENT,
  `IdCenterUser` BIGINT NOT NULL,
  `Status` VARCHAR(45) NOT NULL,
  `CreatedAt` DATETIME NOT NULL,
  `AvailableTo` DATETIME NOT NULL,
  PRIMARY KEY (`IdRequest`),
  INDEX `Request_to_User_FK_idx` (`IdCenterUser` ASC) VISIBLE,
  CONSTRAINT `Request_to_User_FK`
    FOREIGN KEY (`IdCenterUser`)
    REFERENCES `Users` (`IdUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `DonationRequestProducts` (
  `IdRequestProduct` BIGINT NOT NULL AUTO_INCREMENT,
  `IdRequest` BIGINT NOT NULL,
  `ProductName` VARCHAR(255) NOT NULL,
  `Quantity` BIGINT NOT NULL,
  `MeasureUnit` VARCHAR(45) NULL,
  PRIMARY KEY (`IdRequestProduct`),
  INDEX `RequestProduct_to_Request_FK_idx` (`IdRequest` ASC) VISIBLE,
  CONSTRAINT `RequestProduct_to_Request_FK`
    FOREIGN KEY (`IdRequest`)
    REFERENCES `DonationRequests` (`IdRequest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `UserDonationProducts` (
  `IdUserDonationProduct` BIGINT NOT NULL AUTO_INCREMENT,
  `IdDonorUser` BIGINT NOT NULL,
  `IdRequestProduct` BIGINT NOT NULL,
  `Quantity` INT NOT NULL,
  PRIMARY KEY (`IdUserDonationProduct`),
  INDEX `DonationProduct_To_User_FK_idx` (`IdDonorUser` ASC) VISIBLE,
  INDEX `DonationProduct_To_RequestProduct_FK_idx` (`IdRequestProduct` ASC) VISIBLE,
  CONSTRAINT `DonationProduct_To_User_FK`
    FOREIGN KEY (`IdDonorUser`)
    REFERENCES `Users` (`IdUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `DonationProduct_To_RequestProduct_FK`
    FOREIGN KEY (`IdRequestProduct`)
    REFERENCES `DonationRequestProducts` (`IdRequestProduct`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
